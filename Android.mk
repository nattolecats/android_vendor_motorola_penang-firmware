LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),penang)

AB_OTA_PARTITIONS += \
    modem

$(call add-radio-file,modem.img)

include $(call all-makefiles-under,$(LOCAL_PATH))

$(warning $(TARGET_DEVICE) firmware has included!!)

endif
